# Changelog

## [0.9.1] 2020-12-21

- Fixed assigning old paths to copied rows.

## [0.9.0] 2020-12-20

- Added support for browsing HOME and external volumes directories.
- Added support for opening files.
- Added support for multi-selection of files and directories.
- Added support for moving, copying and pasting files and directories.
- Added support for creating of new directories.
- Added support for renaming files and directories.
